import java.util.Arrays;

public class App {
    public static void main(String[] args) throws Exception {
        // task 1: Kiểm tra giá trị truyền vào có phải chuỗi hay không
        String arr1 = "Devcamp";
        int number = 1;
        System.out.println(task1(arr1));
        System.out.println(task1(number));

        // task 2: Cắt n phần tử đầu tiên của chuỗi
        String arr2 = "Robin Singh";
        System.out.println("task 2: " + arr2.substring(0, 4));

        // task 3: Chuyển chuỗi thành mảng các từ trong chuỗi
        String arr3 = "Robin Singh";
        String[] arStr = arr3.split(" ");
        System.out.println("task 3: " + Arrays.toString(arStr));

        // task 4: Chuyển chuỗi về định dạng viết thường
        String arr4 = "Robin Singh from USA";
        String word = arr4.toLowerCase();
        System.out.println("task 4: " + word);

        // task 5: Bỏ các khoảng trắng giữa các từ trong chuỗi, viết hoa chữ cái đầu
        // tiên của mỗi từ
        String input5a = "JavaScript Exercises";
        String input5b = "JavaScript exercises";
        String input5c = "JavaScriptExercises";
        task5(input5a);
        task5(input5b);
        task5(input5c);

        // task 6 : Viết hoa chữ cái đầu tiên mỗi từ
        String arr5 = "js string exercises";
        System.out.println("task 6: " + task6(arr5));

        // task 7 : Lặp lại n lần từ 1 từ cho trước, có khoảng trắng giữa các lần lặp
        String arr6 = "Ha!";
        System.out.println("task 7: " + task7(arr6, 1));
        System.out.println("task 7: " + task7(arr6, 2));
        System.out.println("task 7: " + task7(arr6, 3));

        // task 8:
        String arr7 = "dcresource";
        System.out.println("\nTask 8:");
        task8(arr7, 2);
        task8(arr7, 3);
        String[] result8a = task8(arr7, 2);
        System.out.println(Arrays.toString(result8a));
        System.out.println(Arrays.toString(task8(arr7, 3)));

        // task 9:
        System.out.println("\nTask 9:");
        String str9 = "The quick brown fox jumps over the lazy dog";
        String findStr = "the";
        task9(str9, findStr);

        // task 10: Thay n ký tự bên phải của một chuỗi bởi 1 chuỗi khác
        String str10 = "0000";
        String str10_1 = "123";
        System.out.println("\nTask 10:");
        task10(str10, str10_1);

    }

    // task 1: Kiểm tra giá trị truyền vào có phải chuỗi hay không
    public static Boolean task1(Object ojb) {

        return ojb instanceof String;
    }

    // task 5: Bỏ các khoảng trắng giữa các từ trong chuỗi, viết hoa chữ cái đầu
    // tiên của mỗi từ
    public static void task5(String str) {
        String[] arr = str.split(" ");

        String arr1 = "";
        for (String x : arr) {
            arr1 = arr1 + (x.substring(0, 1).toUpperCase() + x.substring(1));
        }
        System.out.println(arr1);

    }

    // task 6 : Viết hoa chữ cái đầu tiên mỗi từ
    public static String task6(String str) {
        String[] arr = str.split(" ");

        String arr1 = "";
        for (String x : arr) {
            arr1 = arr1 + (x.substring(0, 1).toUpperCase() + x.substring(1));
            arr1 = arr1 + " ";
        }
        return arr1;

    }

    // task 7 :Lặp lại n lần từ 1 từ cho trước, có khoảng trắng giữa các lần lặp
    public static String task7(String str, int n) {
        String arrNew = "";
        for (int i = 0; i < n; i++) {
            arrNew += str + " ";
        }
        return arrNew.trim();

    }
    // task 8 :Đưa chuỗi thành mảng mà mỗi phần tử là n ký tự lần lượt từ chuỗi đó

    public static String[] task8(String str, int n) {
        String[] result = str.split(String.format("(?<=\\G.{%1$d})", n));
        return result;
    }

    // task 9: Đếm số lần xuất hiện của chuỗi con trong 1 chuỗi cho trước
    public static void task9(String str, String n) {
        int result = str.split(n, -1).length;
        System.out.println(result);
    }

    // task 10: Thay n ký tự bên phải của một chuỗi bởi 1 chuỗi khác
    public static void task10(String str, String n){
        String str10 = str.substring(0, str.length() - n.length()) + n;
        System.out.println("Kết quả: " + str10);

    }
}
